const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;
  } else if (/[a-z]/gi.test(str1) || /[a-z]/gi.test(str2)) {
    return false;
  } else if (str1 === '') {
    str1 = 0;
    return String(str1 + +str2);

  } else if (str2 === '') {
    str2 = 0;
    return String(str2 + +str1);
  }
  return String(+str1 + +str2);
};


const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let post = 0;
  let comments = 0;
  listOfPosts.forEach(obj => {
    if (obj.author === authorName) {
      post++;
    }
    for (let key in obj) {
      if (Array.isArray(obj[key])) {
        obj[key].forEach(obj => {
          if (obj.author === authorName) {
            comments++;
          } 
        });
      }
    }

  });
  return `Post:${post},comments:${comments}`;
};

const tickets=(people) => {
  let deposit = [0,0,0];

  function updateDeposit(paid) {
    for(let i=0; i<deposit.length; i++) {
      deposit[i] = deposit[i] + paid[i];
    }
  }

  for (let index in people) {

    if(people[index] === 25) {
      updateDeposit([1,0,0])
    }

    else if(people[index] === 50) {
      updateDeposit([-1,1,0])
    }

    else {
      if (deposit[0] >=1 && deposit[1] >=1){
          updateDeposit([-1,-1,1]);}
      else if (deposit[0] >=3 && deposit[1] == 0){
          updateDeposit([-3, 0, 1]);}
      else {
          updateDeposit([-3, 0, 1]);}
    }

    if (deposit[0] < 0 || deposit[1] < 0 || deposit[2] < 0 ) {
      return 'NO';
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};